class Generador
{
    public static void main(String[] args)
    {
        final String S = "###v%nt%n%";
        System.out.println(S);
        int lenWord = S.length();
        int lenPositionAndTypeToChange=0;

        for(int i=0 ; i<lenWord ; i++)
        {
            char x = S.charAt(i);
            if('%'==x|'#'==x|'{'==x|'}'==x
                    |'&'==x|'.'==x)
            {
                lenPositionAndTypeToChange++;
            }
        }

	if(lenPositionAndTypeToChange==0)
        {
            System.out.println("Nothing to do. You're a BITCH.");
            System.exit(0); //this stop the JVM.
        }

        int[] positionToChange = new int[lenPositionAndTypeToChange];
        char[] typeOfChange = new char[lenPositionAndTypeToChange];
        
        for(int i=0 ; i<lenWord ; i++)
        {
            char x = S.charAt(i);
            if('%'==x|'#'==x|'{'==x|'}'==x
                    |'&'==x|'.'==x)
            {
                positionToChange[i]=x;
		typeOfChange[i]=x;
            }
        }

        System.out.println("Length of the word -> "+lenWord);
        System.out.println(lenPositionAndTypeToChange+" positions to change.");

        char[] numbers = {'0','1','2','3','4','5','6','7','8','9'};
        char[] abc={'a','b','c','d','e','f','g','h','i','j','k',
            'l','m','n','ñ','o','p','q','r','s','t','u','v','w','x','y','z'};
        char[] ABC={'A','B','C','D','E','F','G','H','I','J','K','L','M','Ñ','N'
                ,'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        char[] aeiou={'a','e','i','o','u'};
        char[] AEIOU={'A','E','I','O','U'};
        char[] rare={'[',']','!','¡','?','¿','@','&','#','~','$','&','/','-',
            '-','+','=','_','*','<','>','|','°','¬','{','}','.',':','"','\\','\''};

        int l_numbers=10, i_numbers=0;
        int l_abc=27, i_abc=0;        
        int l_ABC=27, i_ABC=0;
        int l_aeiou=5, i_aeiou=0;
        int l_AEIOU=5, i_AEIOU=0;
        int l_rare=31, i_rare=0;
        
        for(int i=0 ; i<lenPositionAndTypeToChange ; i++)
        {
            System.out.println("position:"+positionToChange[i]+
                    " type:"+typeOfChange[i]);
        }
        
        char[] word = new char[lenWord];
        
        for(int i=0, o=0 ; i<lenWord ; i++)
        {
            if(i==positionToChange[o])
            {
                switch(typeOfChange[o++])
                {
                    case '#': word[i]=numbers[0]; break;
                    case '{': word[i]=abc[0]; break;
                    case '}': word[i]=ABC[0]; break;
                    case '&': word[i]=aeiou[0]; break;
                    case '.': word[i]=AEIOU[0]; break;
                    case '%': word[i]=rare[0]; break;
                }
            }
            else
            {
                word[i]=S.charAt(i);
            }
        }
        
        for(int i=0 ; i<lenWord ; i++)
        {
            System.out.print(word[i]);
        }
        System.out.println("");
    }
}
